# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is the repository storing al the part, figures and note of the book I am writing for the Physics of
the Atmosphere and the Atmospheric Boundary Layer courses I am teaching at University of Trieste in the 
frame of Master of Physics

Version 0.1

### Content ###

This repository is composed of the following subdirectories:

1. Figures ---> Here all the figures of the book are stored in sundirectories
                and their file names are composed of five alfanumerich characters
                folleved by an unerdcore, then tre characters that are a progressive
                integer number. Finally the kind of image fiel extension. Exemples are:
    
                CH001_023.jpg N01F2_000.gif AA111_198.ps
    
                In the directory there is a YAML file that reports the code of the file name
                the sub directory where it is stored and the some metadata such as the source
                of the image, the motivations and purposes of the image, the copyright citation
                and all the stuff useful for image interpretation

2. Notes   ---> Here are files with notes and text that will become part of the book. Mainly they are in MS Word 
                of other formats that are suitable for some text editor in which a word corrector or a dictionary 
	        is available.

3. Biblio  ---> Here are files concerning the bibliography. Mainly the bibtex file are there, but also notes on 
                the books or cited articles may be in.

4. Text    ---> Here are LaTEX file that are related to the book chapter 


By Dario B. Giaiotti
dgiaiotti@units.it

Last update: March 03, 2019
